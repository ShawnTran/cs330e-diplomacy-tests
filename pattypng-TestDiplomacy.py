# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, checkForAttackedSupport, makeAlphabetical, citiesBeingSupported, diplomacy_eval


# -----------
# TestDiplomacy

# -----------

class TestDiplomacy (TestCase):

    # -----
    # start
    # -----

    def test_solve1(self):
        r = StringIO("A Tampico Hold\nB Austin Move Tampico\nC Houston Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Tampico\nC Houston")

    def test_solve2(self):
        r = StringIO("A Monterrey Hold\nB CiudadJuarez Move Monterrey\nC Acapulco Move Monterrey\nD Cancun Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Cancun\nE Austin")

    def test_solve3(self):
        r = StringIO("A Chihuahua Hold\nB Monterrey Move Chihuahua\nC Berlin Support B\nD Austin Move Berlin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]")
    
    def test_solve4(self):
        r = StringIO("A Tampico Hold\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A Tampico")  

    def test_solve5(self):
        r = StringIO("A Tampico Hold\nB Austin Move Tampico\nC Houston Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Tampico\nC Houston")
        

  

#

if __name__ == "__main__": #pragma: no cover
    main()
