from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):

    # 3 tests for the function diplomacy_solve()
    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        )
    
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n"
        )
    
    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
        )
    
    def test_solve4(self):
        r = StringIO("A Madrid Move London\nB Barcelona Move Madrid\nC London Move Barcelona")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A London\nB Madrid\nC Barcelona\n"
        )
    
    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Tokyo Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Tokyo\n"
        )
    
    def test_solve6(self):
        r = StringIO("A London Hold\nB Madrid Move London")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n"
        )

    def test_solve7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Tokyo Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Tokyo\n"
        )
    
    def test_solve8(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF Houston Support E")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\nF Houston\n"
        )



if __name__ == "__main__":
    main()
